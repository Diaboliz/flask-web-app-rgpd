DROP TABLE IF EXISTS submits;

CREATE TABLE submits (
    id int primary key auto_increment,
    firstname varchar(150) not null,
    lastname varchar(150) not null,
    birthdate varchar(150) not null,
    birthplace varchar(150) not null,
    degreelvl varchar(150) not null,
    postaladdress varchar(150) not null,
    mail varchar(150) not null,
    socSecNumber int not null,
    phonenumber varchar(15) not null,
    created date not null default now()
);