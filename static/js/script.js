document.getElementById("birthdate").addEventListener('input', (event) => {
    var label = document.getElementById("label-birthdate");
    var elmt = document.getElementById("birthdate");
    var val = event.target.value;
    
    if (val.length == 2 || val.length == 5) {
        console.log("Trigger len 2/5");
        elmt.value += "/";
    }
    else if ((val.length == 3 || val.length == 6) && val[val.length-1] != '/')
    {
        console.log("Trigger reappend")
        elmt.value = val.substring(0, val.length-1) + "/" + val[val.length-1]
    }
    else if (val.length == 3 || val.length == 6)
    {
        console.log("Trigger len 3/6");
        elmt.value = val.substring(0, val.length-1);
    }

    if(/^((0[1-9])|((1|2)[0-9])|(3(0|1)))\/((0[1-9])|(1[0-2]))\/((19[0-9]{2})|(20(0|1)[0-9]{1}))$/.test(val)) {
        label.textContent = "Date de naissance";
        event.target.style.border = "none";
    }
    else
    {
        label.innerHTML = `Date de naissance <span style="color: red; font-style: italic;"> - Invalid</span>`;
        event.target.style.border = "1px solid red";
    }
});

document.getElementById("mail").addEventListener("input", (event) => {
    var label = document.getElementById("label-mail");
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(event.target.value.trim()) && label.textContent.includes("Invalid"))
    {
        label.textContent = "Adresse mail";
        event.target.style.border = "none";
    }
    else
    {
        label.innerHTML = `Adresse mail <span style="color: red;font-style: italic;"> - Invalid</span>`;
        event.target.style.border = "1px solid red";
    }
});

document.getElementById("socSecNumber").addEventListener("input", (event) => {
    var label = document.getElementById("label-ssn");
    var val = event.target.value.trim();
    if (val.length != 15 || (val[0] != "1" && val[0] != "2"))
    {
        label.innerHTML = `Sécurité sociale <span style="color: red;font-style: italic;"> - Invalid</span>`;
        event.target.style.border = "1px solid red";
    }
    else
    {
        label.textContent = "Sécurité sociale";
        event.target.style.border = "none";
    }
});

document.getElementById("phonenumber").addEventListener("input", (event) => {
    var label = document.getElementById("label-phonenb");
    var val = event.target.value.trim();
    val = val.replaceAll(' ', '');
    console.log(val)
    if ((val[0] == "+" && (val.substring(3).length == 9) && isInt(val.substring(1))) || (val.length == 10 && val[0] == "0" && isInt(val)))
    {
        label.textContent = "Téléphone";
        event.target.style.border = "none";
    }
    else
    {
        label.innerHTML = `Téléphone <span style="color: red;font-style: italic;"> - Invalid</span>`;
        event.target.style.border = "1px solid red";
    }
});

function isInt(d) {
    for (var i = 0; i < d.length; i++) {
        if (!(d[i] >= "0" && d[i] <= "9"))
        {
            return false;
        }
    }
    return true;
}