#coding: utf-8

import mysql.connector

import click

# Installation conencteur MySQL: python -m pip install mysql-connector-python
# https://gayerie.dev/docs/python/python3/mysql.html


def get_db_params() -> dict:
    return {
        "host": "localhost",
        "user": "login",
        "password": "pswd",
        "data_base": "applications_db"
    }


def insert_submit(fname: str, lname: str, bdate: str, bplace: str, degree: str, postaladdress: str, mail: str, ssn: int, phonenb: str):
    query = """insert into submits
            (firstname, lastname, birthdate, birthplace, degreelvl, postaladdress, mail, ssn, phonenumber)
            values (%s, %s, %s, %s, %s, %s, %s, %s, %s)"""
    values = (fname, lname, bdate, bplace, degree,
              postaladdress, mail, ssn, phonenb)

    with mysql.connector.connect(**get_db_params()) as db:
        with db.cursor() as crsr:
            crsr.execute(query, values)
            db.commit()

            print("Transaction done")


def fetch_data(id: int) -> dict:
    query = "select * from submits where id=%s"
    with mysql.connector.connect(**get_db_params()) as db:
        with db.cursor() as crsr:
            crsr.execute(query, (id))
            ks = ("firstname", "lastname", "birthdate", "birthplace",
                  "degreelvl", "postaladdress", "mail", "ssn", "phonenumber")
            rs = crsr.fetchall()
            return {k: v for k, v in zip(ks, rs)}
