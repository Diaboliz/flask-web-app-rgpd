# coding: utf-8

import os
import re

from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/', methods=["POST"])
def submit_form():

    fname = request.form.get("fname")
    lname = request.form.get("lname")
    birthdate = request.form.get("birthdate")
    birthplace = request.form.get("placeBirth")
    degree = request.form.get("degreeLvl")
    mail = request.form.get("mail")
    address = request.form.get("address")
    ssn = request.form.get("socSecNumber")
    phoneNumber = request.form.get("phonenumber").strip().replace(" ", "")

    submissionValid = True

    if not re.match("^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$", mail):
        submissionValid = False

    if len(ssn) != 15 or (ssn[0] != "1" and ssn[0] != "2"):
        submissionValid = False

    if not ((phoneNumber[0] == "+" and (len(phoneNumber) == 12 or len(phoneNumber) == 11 or len(phoneNumber) == 13)) or (len(phoneNumber) == 10 and phoneNumber[0] == "0")):
        submissionValid = False

    print(request.form)
    return render_template('submittedForm.html')


if __name__ == "__main__":
    app.run(host='192.168.1.24', port=5000, debug=True)
